package at.htld.oatjump;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Main extends BasicGame
{
	private Player player;
	private OatCounter oatCounter;
	public boolean restart = false;
	
	private List <Platform> platforms;
	private List <Oat> oats;
	
	private float screenShift = 900;
	private float score = 0;
	
	public List<Platform> generatePlatforms()
	{
		List<Platform> temp = new ArrayList();
		
		Random rnd = new Random();
		
		int greencount = 0;
		int bluecount = 0;
		int yellowcount = 0;
		
		int count = 13 + rnd.nextInt(5);
		
		greencount = (int) (rnd.nextFloat() * count);
		count -= greencount;
		
		if(count != 0)
		{
			bluecount = (int) (rnd.nextFloat() * count);
			count -= bluecount;
		}
		
		if(count != 0)
		{
			yellowcount = (int) (rnd.nextFloat() * count);
			count -= yellowcount;
		}
		
		for(int i = 0; i < greencount; i++)
		{
			temp.add(new PlatformGreen(rnd.nextInt(500), rnd.nextInt(900) - 900));
		}
		
		for(int i = 0; i < bluecount; i++)
		{
			temp.add(new PlatformBlue(rnd.nextInt(500), rnd.nextInt(900) - 900));
		}
		
		for(int i = 0; i < yellowcount; i++)
		{
			temp.add(new PlatformYellow(rnd.nextInt(500), rnd.nextInt(900) - 900));
		}
		
		if(count != 0)
		{
			for(int i = 0; i < count; i++)
			{
				temp.add(new PlatformMagenta(rnd.nextInt(500), rnd.nextInt(900) - 900));
			}		
		}
			
		return temp;
	}
	
	public Main()
	{
		super("Oatjump");
	}
	
	@Override
	public void render(GameContainer container, Graphics g) throws SlickException
	{
		player.render(g);
		
		for(Platform platforms:platforms)
		{
			platforms.render(g);
		}
		
		for(Oat oat:oats)
		{
			oat.render(g);
		}
		
		g.setBackground(Color.lightGray);
		
		//showing score and instruction to restart on screen
		g.setColor(Color.black);
		g.drawString("Score: " + Math.round(this.score), 10, 10);
		g.drawString("Oats:  " + oatCounter.count, 10, 25);
		g.drawString("press R to restart", 430, 10);
	}

	@Override
	public void init(GameContainer container) throws SlickException
	{
		//starting position of the player
		this.player = new Player(300, 800);
		
		this.platforms = new ArrayList();
		
		//platforms at the beginning
		platforms.add(new PlatformGreen(0, 100));
		platforms.add(new PlatformGreen(100, 300));
		platforms.add(new PlatformGreen(300, 200));
		platforms.add(new PlatformGreen(100, 500));
		platforms.add(new PlatformGreen(500, 700));
		platforms.add(new PlatformGreen(300, 400));
		platforms.add(new PlatformGreen(300, 810));
		
		this.oats = new ArrayList();
		oats.add(new Oat(200, 400));
		
		oatCounter = new OatCounter();
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		restart = container.getInput().isKeyDown(Input.KEY_R);
		
		player.update(container, delta, platforms, oats, oatCounter, restart);
		
		for(Platform platform:platforms)
		{
			platform.update(delta, player);
			platform.shift(player.setShift);
			
			//delete platform if out of sight at the bottom
			if(platform.getShape().getY() > 900)
			{
				platform.setDelete();
			}
		}
		
		for(int i = 0; i < platforms.size(); i++)
		{
			if(platforms.get(i).getDelete())
			{
				platforms.remove(i);
				i--;
			}
		}
		
		score += player.setShift;
		screenShift += player.setShift;
		player.setShift = 0;		
		
		if(screenShift>900)
		{
			platforms.addAll(this.generatePlatforms());
			screenShift = 0;
		}
	}

	public static void main(String[] argv)
	{
		try
		{
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(600, 900, false);
			container.setShowFPS(false);
			container.start();
		} 
		catch (SlickException e)
		{
			e.printStackTrace();
		}
	}
}
