package at.htld.oatjump;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class PlatformGreen implements Platform
{
	private Rectangle platform;
	private boolean collision = false;
	private boolean delete = false;
	
	public void setDelete()
	{
		delete = true;
	}
	
	public String getType()
	{
		return "green";
	}
	
	public boolean getDelete()
	{
		return delete;
	}
	
	public boolean getCollision()
	{
		return collision;
	}
	
	public void setCollision()
	{
		collision = true;
	}

	public PlatformGreen(int x, int y)
	{
		super();
		platform = new Rectangle(x, y, 100, 20);
	}

	public void update(int delta, Player player)
	{

	}
	
	public void shift(float amount)
	{
		platform.setY(platform.getY() + amount);
	}

	public void render(Graphics g)
	{
		g.setColor(Color.green);
		g.fillRoundRect(platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight(),90);
	}

	@Override
	public Shape getShape()
	{
		return this.platform;
	}
}