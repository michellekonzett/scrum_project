package at.htld.oatjump;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public interface Platform
{
	void setCollision();	//if collision true, "jump"
	boolean getCollision();	//collision true or false
	boolean getDelete();	//delete true or false
	void setDelete();	//if delete = true, delete platform	
	String getType(); 	//returns color
	
	void render(Graphics g);

	void update(int delta, Player player);
	
	void shift(float amount); //background scrolling for jumping illusion
	
	Shape getShape();	//get the Shape of the platform
}
