package at.htld.oatjump;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class PlatformBlue implements Platform
{
	private Rectangle platform;
	private int movement;
	private boolean collision = false;
	private boolean delete = false;
	
	public void setDelete()
	{
		delete = true;
	}
	
	public String getType()
	{
		return "blue";
	}
	
	public boolean getDelete()
	{
		return delete;
	}
	
	public boolean getCollision()
	{
		return collision;
	}
	
	public void setCollision()
	{
		collision = true;
	}
	
	public PlatformBlue(int x, int y)
	{
		super();
		platform = new Rectangle(x, y, 100, 20);
	}

	public void update(int delta, Player player)
	{
		//moving left and right
		if (movement == 0)
		{
			platform.setX(platform.getX()+0.3f);
			if (platform.getX() >= (600-platform.getWidth()))
			{
				movement = 1;
			}
		}
		if (movement == 1)
		{
			platform.setX(platform.getX()-0.3f);
			if (platform.getX() <= 0)
			{
				movement = 0;
			}
		}
	}
	
	public void shift(float amount)
	{
		platform.setY(platform.getY() + amount);
	}

	public void render(Graphics g)
	{
		g.setColor(Color.blue);
		g.fillRoundRect(platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight(),90);
	}

	@Override
	public Shape getShape()
	{
		return this.platform;
	}
}