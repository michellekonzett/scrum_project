package at.htld.oatjump;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class PlatformMagenta implements Platform
{
	private Rectangle platform;
	private int movement;
	private float spawnpoint;
	private boolean collision = false;
	private boolean delete = false;
	
	public void setDelete()
	{
		delete = true;
	}
	
	public String getType()
	{
		return "magenta";
	}
	
	public boolean getDelete()
	{
		return delete;
	}
	
	public boolean getCollision()
	{
		return collision;
	}
	
	public void setCollision()
	{
		collision = true;
	}

	public PlatformMagenta(int x, int y)
	{
		super();
		platform = new Rectangle(x, y, 100, 20);
		spawnpoint = y;
	}
	
	public void update(int delta, Player player)
	{
		//moving up and down
		if (movement == 0)
		{
			platform.setY(platform.getY()+0.3f);
			if (platform.getY() >= (spawnpoint+100))
			{
				movement = 1;
			}
		}
		if (movement == 1)
		{
			platform.setY(platform.getY()-0.3f);
			if (platform.getY() <= (spawnpoint-100))
			{
				movement = 0;
			}
		}
	}
	
	public void shift(float amount)
	{
		platform.setY(platform.getY() + amount);
		spawnpoint += amount;
	}

	public void render(Graphics g)
	{
		g.setColor(Color.magenta);
		g.fillRoundRect(platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight(),90);
	}

	@Override
	public Shape getShape()
	{
		return this.platform;
	}
}