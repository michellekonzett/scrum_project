package at.htld.oatjump;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class Oat
{
	private double x, y;
	private Image oatImage;
	public Rectangle oat;

	public Oat(int x, int y) throws SlickException
	{
		super();
		this.x = x;
		this.y = y;
		oat = new Rectangle((int)x, (int)y, 45, 72);
		oatImage = new Image("testdata/OatJump/Oat.png");
	}

	public void update(GameContainer container, int delta)
	{

	}

	public void render(Graphics g)
	{
		oatImage.draw((float) oat.getX(), (float) oat.getY(), oat.getWidth(), oat.getHeight());
	}
}
