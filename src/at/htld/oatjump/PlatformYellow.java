package at.htld.oatjump;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class PlatformYellow implements Platform
{
	private Rectangle platform;
	private int stage;
	private boolean collision = false;
	private boolean delete = false;
	private long timeCreated;
	private Color renderColor;
	
	Random rnd;
	int timeAlive;
	
	public void setDelete()
	{
		delete = true;
	}
	
	public String getType()
	{
		return "yellow";
	}
	
	public boolean getDelete()
	{
		return delete;
	}
	
	public boolean getCollision()
	{
		return collision;
	}
	
	public void setCollision()
	{
		collision = true;
	}

	public PlatformYellow(int x, int y)
	{
		super();
		platform = new Rectangle(x, y, 100, 20);
		timeCreated = System.currentTimeMillis();		
		renderColor = Color.yellow;
		
		rnd = new Random();
		timeAlive = 2000 + rnd.nextInt(1500);
	}
	
	public void update(int delta, Player player)
	{
		//change color to red, then delete platform
		if(System.currentTimeMillis() - timeCreated > (1000 + timeAlive))
		{
			renderColor = Color.red;
		}
		if(System.currentTimeMillis() - timeCreated > (1000 + 2 * timeAlive))
		{
			delete = true;
		}			
	}
	
	public void shift(float amount)
	{
		platform.setY(platform.getY() + amount);
	}

	public void render(Graphics g)
	{
		if(stage == 0)
		{
			g.setColor(renderColor);
		}
		if(stage == 1)
		{
			g.setColor(renderColor);
		}
		g.fillRoundRect(platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight(),90);
	}

	@Override
	public Shape getShape()
	{
		return this.platform;
	}
}