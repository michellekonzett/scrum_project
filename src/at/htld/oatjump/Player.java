package at.htld.oatjump;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import java.util.ArrayList;
import java.util.List;

public class Player
{
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean shoot = false;
	private boolean death = false;
	public boolean platformCollision = false;
	public boolean sideCollision = false;
	private double ySpeed = 0;	
	public float setShift = 0;	
	private Rectangle player;
	private Image playerImage;
	
	public Player(int x, int y) throws SlickException
	{
		super();
		player = new Rectangle(x, y, 64, 43);
		playerImage = new Image("testdata/OatJump/Characters/Brother/Ansichten/right.png");
	}

	public void update(GameContainer container, int delta, List<Platform> platforms/*, List<Enemy> enemies*/, List<Oat> oats, OatCounter counter, boolean restart) throws SlickException
	{
		//set start position after a restart
		if(restart == true)
		{
			player.setX(300);
			player.setY(800);
			death = false;
		}

		moveleft = container.getInput().isKeyDown(Input.KEY_LEFT) | container.getInput().isKeyDown(Input.KEY_A);
		moveright = container.getInput().isKeyDown(Input.KEY_RIGHT) | container.getInput().isKeyDown(Input.KEY_D);
		shoot = container.getInput().isKeyDown(Input.KEY_SPACE) | container.getInput().isKeyDown(Input.KEY_W);

		if (moveleft == true)
		{	
			//horizontal png
			player.setWidth(64);
			player.setHeight(43);
			
			//only change picture if it isn't "left" yet or right or shoot isn't true too
			if(playerImage.getName() != "left" && moveright == false && shoot == false)
			{
				playerImage.destroy();
				playerImage = new Image("testdata/OatJump/Characters/Brother/Ansichten/left.png");
				playerImage.setName("left");
			}
			
			//movement
			player.setX(player.getX()-1);

			//coming in on the other side of the screen again
			if (player.getX() <= 0-player.getWidth())
			{
				player.setX(599);
			}
		}
		
		if (moveright == true)
		{
			//horizontal png
			player.setWidth(64);
			player.setHeight(43);
			
			//only change picture if it isn't "right" yet or left or shoot isn't true too
			if(playerImage.getName() != "right" && moveleft == false && shoot == false)
			{
				playerImage.destroy();
				playerImage = new Image("testdata/OatJump/Characters/Brother/Ansichten/right.png");
				playerImage.setName("right");
			}
			
			//movement
			player.setX(player.getX()+1);
			
			//coming in on the other side of the screen again
			if (player.getX() >= 600)
			{
				player.setX(1-player.getWidth());
			}
		}
		
		if(shoot == true)
		{
			//vertical png
			player.setWidth(43);
			player.setHeight(67);
			
			//only change picture if it isn't "shoot" already
			if(playerImage.getName() != "shoot")
			{
				playerImage.destroy();
				playerImage = new Image("testdata/OatJump/Characters/Brother/Ansichten/shoot.png");
				playerImage.setName("shoot");
			}		
		}
		
		ySpeed = ySpeed + 0.001f;

		player.setY(player.getY() + (float) ySpeed);
	
		if (player.getY() >= 900)
		{
			death = true;
		}
				
//		for(Enemy enemy : enemies)
//		{
//			if(player.intersects(enemy.getRectangle()))
//			{
//				sidecollision = true;
//				playerImage = new Image("testdata/supermeatboy/meatboy/dead.png");
//				death = true;
//			}
//		}
		
		Oat temp = null;
		for(Oat oat : oats)
		{
			if(player.intersects(oat.oat))
			{
				sideCollision = true;
				temp = oat;
				counter.count ++;
				System.out.println("Oats: " + counter.count);
			}
		}
		if(temp != null)
		{
			oats.remove(temp);
		}
		
		if(ySpeed >= 0)
		{
			for(Platform platform : platforms)
			{
				if(player.intersects(platform.getShape()))
				{
					platform.setCollision();
					if(platform.getType() != "black") ySpeed = -0.75;
				}			
			}
		}	
		
		if (player.getY() < 450 )
		{
			this.setShift = 450 - player.getY();
			player.setY(450);
		}
	}

	public void render(Graphics g)
	{
		playerImage.draw((float) player.getX(), (float) player.getY(), player.getWidth(), player.getHeight());
	}
}